package devcamp.pizza.controller;

import java.util.ArrayList;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import devcamp.pizza.model.CVoucher;

import devcamp.pizza.repository.IVoucherRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CVoucherController {
    @Autowired
    IVoucherRepository pIVoucherRepository;




    @GetMapping("/voucher")
    public ResponseEntity<java.util.List<CVoucher>> getAllVoucher() {
        try {
            java.util.List<CVoucher> pCVouchers = new ArrayList<CVoucher>()  ;
            pIVoucherRepository.findAll().forEach(pCVouchers :: add);
            return new ResponseEntity<>(pCVouchers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }
    
}
