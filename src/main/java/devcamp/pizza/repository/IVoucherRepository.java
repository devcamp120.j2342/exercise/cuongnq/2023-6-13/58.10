package devcamp.pizza.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.pizza.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher , Long> {
    
}
